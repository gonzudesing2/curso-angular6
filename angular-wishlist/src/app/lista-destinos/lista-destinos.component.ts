import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { DestinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';

@Component({
	selector: 'app-lista-destinos',
	templateUrl: './lista-destinos.component.html',
	styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

	@Output() ItemAdded: EventEmitter<DestinoViaje>;
	updates: string[];

	constructor(private destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
		this.ItemAdded = new EventEmitter();
		this.updates = [];
		this.store.select(state => state.destinos.favorito).subscribe(data => {
			if (data != null) {
				this.updates.push('Se ha elegido a ' + data.nombre);
			}
		});
	}

	ngOnInit(): void {
	}

	agregado(d: DestinoViaje): void {
		this.destinosApiClient.add(d);
		this.ItemAdded.emit(d);
	}

	elegido(d: DestinoViaje): void {
		this.destinosApiClient.elegir(d);
	}

	eliminado(d: DestinoViaje): void {
		this.destinosApiClient.remove(d);
	}

	getAll(): DestinoViaje[] {
		return this.destinosApiClient.getAll();
	}

	resetearVotos(): boolean {
		this.destinosApiClient.resetAllVotes();
		return false;
	}

}
