import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { DestinoViaje } from './destino-viaje.model';
import { ElegidoFavoritoAction, EliminadoDestinoAction, NuevoDestinoAction, VoteResetAllAction } from './destinos-viajes-state.model';

@Injectable()
export class DestinosApiClient {
	destinos: DestinoViaje[];

	constructor(private store: Store<AppState>) {
		this.store.select(state => state.destinos).subscribe(data => {
			this.destinos = data.items;
		});
	}

	add(d: DestinoViaje): void {
		this.store.dispatch(new NuevoDestinoAction(d));
	}

	remove(d: DestinoViaje): void {
		this.store.dispatch(new EliminadoDestinoAction(d));
	}

	getAll(): DestinoViaje[] {
		return this.destinos;
	}

	getById(id: string): DestinoViaje {
		return this.destinos.find(d => d.id.toString() === id);
	}

	elegir(d: DestinoViaje): void {
		this.store.dispatch(new ElegidoFavoritoAction(d));
	}

	resetAllVotes(): void {
		this.store.dispatch(new VoteResetAllAction());
	}

}
