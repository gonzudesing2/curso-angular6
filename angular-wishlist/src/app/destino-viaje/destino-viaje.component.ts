import { Component, OnInit, Input, Output, HostBinding, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { VoteDownAction, VoteUpAction } from '../models/destinos-viajes-state.model';
import { DestinoViaje } from './../models/destino-viaje.model';

/* tslint:disable:no-input-rename */
@Component({
	selector: 'app-destino-viaje',
	templateUrl: './destino-viaje.component.html',
	styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {

	@Input() destino: DestinoViaje;
	@Input('idx') position: number;
	@HostBinding('attr.class') cssClass = 'col-md-4 py-4';
	@Output() clicked: EventEmitter<DestinoViaje>;
	@Output() deleted: EventEmitter<DestinoViaje>;

	constructor(private store: Store<AppState>) {
		this.clicked = new EventEmitter();
		this.deleted = new EventEmitter();
	}

	ngOnInit(): void {
	}

	ir(): boolean {
		this.clicked.emit(this.destino);
		return false;
	}

	voteUp(): boolean {
		this.store.dispatch(new VoteUpAction(this.destino));
		return false;
	}

	voteDown(): boolean {
		this.store.dispatch(new VoteDownAction(this.destino));
		return false;
	}

	eliminar(): boolean {
		this.deleted.emit(this.destino);
		return false;
	}

}
